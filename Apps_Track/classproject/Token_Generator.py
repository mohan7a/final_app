import os
import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()


def gen_tok():
    from rest_framework.authtoken.models import Token
    from django.contrib.auth.models import User
    user = User.objects.all()

    for usr in user:
        token = Token.objects.create(user=usr)
        print(token.key)


if __name__ == '__main__':
    gen_tok()
import os
import django
import openpyxl as op
import click
from bs4 import BeautifulSoup
from openpyxl import load_workbook
from openpyxl import Workbook

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'classproject.settings')
django.setup()

from onlineapp.models import *

@click.group()
def cli():
    pass


@cli.command()
@click.option('-xl')#,prompt = 'Enter the name of excel file : ')
@click.option('-h')#,prompt = 'Enter the name of html file :')
def importData(h):
    WB = op.load_workbook(filename=xl)
    CollegeSheet = WB["Colleges"]

    for i in range(2, CollegeSheet.max_row + 1):
        try:
            record = College.objects.get(acronym=CollegeSheet.cell(row=i, column=2).value)
        except Exception :
            record = College(name=CollegeSheet.cell(row=i, column=1).value,
                             acronym=CollegeSheet.cell(row=i, column=2).value,
                             location=CollegeSheet.cell(row=i, column=3).value,
                             contact=CollegeSheet.cell(row=i, column=4).value)
            record.save()

    CurrentStudentSheet = WB["Current"]

    for i in range(2, CurrentStudentSheet.max_row + 1):
        try:
            Student.objects.get(name=CurrentStudentSheet.cell(row=i, column=1).value)
        except Exception as e:
            record = Student(name=CurrentStudentSheet.cell(row=i, column=1).value,
                             college=College.objects.get(acronym=CurrentStudentSheet.cell(row=i, column=2).value),
                             email=CurrentStudentSheet.cell(row=i, column=3).value,
                             db_folder=(CurrentStudentSheet.cell(row=i, column=4).value).lower(),
                             dropped_out=False)
            record.save()

    DroppedoutStudentSheet = WB["Deletions"]

    for i in range(2, DroppedoutStudentSheet.max_row + 1):
        try:
            Student.objects.get(name=CurrentStudentSheet.cell(row=i, column=1).value)
        except Exception as e:
            record = Student(name=CurrentStudentSheet.cell(row=i, column=1).value,
                             college=College.objects.get(acronym=CurrentStudentSheet.cell(row=i, column=2).value),
                             email=CurrentStudentSheet.cell(row=i, column=3).value,
                             db_folder=(CurrentStudentSheet.cell(row=i, column=4).value).lower(),
                             dropped_out=True)
            record.save()

    with open(h) as fp:
        soup = BeautifulSoup(fp, features="html.parser")
    p1, p2, p3, p4, t, stu = 0, 0, 0, 0, 0, ''
    table = soup.table
    table = soup.find('table')
    table_rows = table.find_all('tr')
    items = []
    for tr in table_rows:
        td = tr.find_all('td')
        row = [i.text for i in td]
        row = row[1:]
        items.append(row)
    items = items[1:]
    rows = len(items)
    columns = len(items[0])
    for i in range(1, rows + 1):
        for j in range(1, columns + 1):
            if(j==1):
                stu = items[i-1][j-1]
            elif(j==2):
                p1 = items[i-1][j-1]
            elif(j==3):
                p2 = items[i-1][j-1]
            elif(j==4):
                p3 = items[i-1][j-1]
            elif(j==5):
                p4 = items[i-1][j-1]
            else:
                t = items[i-1][j-1]
            #print(items[i-1][j-1],end = ' ')
        #print(stu,' ',p1,' ',p2,' ',p3,' ',p4,' ',t)
        #print(stu.split('_')[2])
        try:
            MockTest1.objects.get(student = (stu.split('_')[2]).lower())
            #print((stu.split('_')[2]).lower(),' - ', 'found')
        except Exception as e:
            #print('not found')
            #print((stu.split('_')[2]).lower(), ' - ', 'not found')
            try:
                Student.objects.get(db_folder=(stu.split('_')[2]).lower())
                record = MockTest1(student = Student.objects.get(db_folder = (stu.split('_')[2]).lower()),
                                problem1 = p1,
                               problem2 = p2,
                               problem3 = p3,
                               problem4 = p4,
                               total = t

                                )
            except:
                print((stu.split('_')[2]).lower(), ' - ', 'not found')
            record.save()

if __name__ == '__main__':
    importData()
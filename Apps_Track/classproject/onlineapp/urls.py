from onlineapp.views import *
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

urlpattern = [
    path('colleges/', college_list.as_view()),
    path('colleges/<int:pk>',college_list.as_view()),
    path('colleges/<str:acronym>', college_list.as_view()),
    path('addcollege/<str:acronym>', addf),
    path('delcollege/<str:acronym>', delf),
    path('addcollege',addf),
    path('colleges/add_st/<int:cid>', add_st_form),
    path('colleges/edit_st/<int:cid>/<int:sid>', add_st_form),
    path('colleges/del_st/<int:cid>/<int:sid>', del_st_form),
    path('login_user/',auth.as_view()),
    path('logout', log_out),
    path('Signup/', signup.as_view()),
    path('college_api/', CollegeAPI),
    path('college_api/<str:acronym>', CollegeAPI),
    path('student_api/', StudentAPI),
    path('student_api/<str:db_folder>', StudentAPI),
    path('mocktest_api/', MockTestAPI),
    path('mocktest_api/<int:student_id>', MockTestAPI),
    path('test/', test_view),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
]



from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect,render
from django.views import View
from django.contrib.auth.models import User
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class auth(View):
    def get(self,request):
        return render(request, 'Login_Page.html', {'error_message': ''})

    def post(self, request, *args, **kwargs):
        usernme = request.POST['username']
        passwrd = request.POST['password']
        usr = authenticate(request, username=usernme, password=passwrd)
        if usr is not None:
            login(request, user=usr)
            return redirect('/colleges/')
        else:
            mesg = 'Invalid Username/Password'
            return render(request, 'Login_Page.html', {'error_message': mesg})


class signup(View):
    def get(self, request):
        return render(request, 'SignUp_Page.html')

    def post(self, request):
        if User.objects.filter(username=request.POST['username']).exists():
            return render(request, 'SignUp_Page.html')
        if User.objects.filter(email=request.POST['email']).exists():
            return render(request, 'SignUp_Page.html')
        usr = User.objects.create_user(request.POST['username'],request.POST['email'], request.POST['password'])
        usr.first_name = request.POST['first_name']
        usr.last_name = request.POST['last_name']
        usr.save()
        login(request,user=usr)
        return redirect('/colleges/')


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def log_out(request):
    logout(request)
    return redirect('/login_user/')
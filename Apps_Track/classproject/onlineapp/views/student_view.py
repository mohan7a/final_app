from django.shortcuts import render
from django.forms import ModelForm
from onlineapp.models import Student,MockTest1
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required


class student_form(ModelForm):
    class Meta:
        model = Student
        fields = ['name','email','db_folder','dropped_out']


class mockTest_form(ModelForm):
    class Meta:
        model=MockTest1
        fields=['problem1','problem2','problem3','problem4']


@login_required(login_url='/login_user/')
def add_st_form(request, *args, **kwargs):
    sform = student_form(request.POST or None)
    mform = mockTest_form(request.POST or None)
    title = 'Add Student'
    submit = 'Add Student'
    if len(kwargs)>1:
        title = 'Confirm Edit'
        submit = 'Confirm Edit'
        scoll = Student.objects.get(id=kwargs['sid'])
        sform = student_form(request.POST or None, instance=scoll)
        mcoll = MockTest1.objects.get(student_id=kwargs['sid'])
        mform=mockTest_form(request.POST or None,instance=mcoll)
    if sform.is_valid() and mform.is_valid():
        student=sform.save(commit=False)
        student.college_id=kwargs['cid']
        student.save()

        mock = mform.save(commit=False)
        mock.total=sum(mform.cleaned_data.values())
        mock.student_id = student.id
        mock.save()

        return HttpResponseRedirect('/colleges/')

    return render(request, 'Modify_Student.html', {'context_instance':RequestContext(request),'sform': sform,'mform':mform, 'action': submit, 'title': title})


@login_required(login_url='/login_user/')
def del_st_form(request, *args, **kwargs):

    title = 'Delete Student'
    submit = 'Delete Student'

    scoll = Student.objects.get(id=kwargs['sid'])
    sform = student_form(request.POST or None, instance=scoll)
    mcoll = MockTest1.objects.get(student_id=kwargs['sid'])
    mform=mockTest_form(request.POST or None,instance=mcoll)

    if sform.is_valid() and mform.is_valid():
        student = sform.save(commit=False)
        student.college_id=kwargs['cid']
        scoll.delete()

        return HttpResponseRedirect('/colleges/')

    return render(request, 'Modify_Student.html', {'context_instance':RequestContext(request),'sform': sform,'mform':mform, 'action': submit, 'title': title})



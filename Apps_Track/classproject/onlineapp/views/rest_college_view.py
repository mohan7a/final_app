from rest_framework.decorators import api_view, authentication_classes, permission_classes
from onlineapp.views.serializers import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication,BasicAuthentication


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes((BasicAuthentication,))
@permission_classes((IsAuthenticated,))
def CollegeAPI(request, *args, **kwargs):
    if request.method == 'GET':
        c = College.objects.all()
        if kwargs:
            c = College.objects.get(**kwargs)
            ser = College_Serializer(c)
        else:
            ser = College_Serializer(c, many=True)
        return Response(ser.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        serializer = College_Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        c = College.objects.get(**kwargs)
        serializer = College_Serializer(c, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes((BasicAuthentication,))
@permission_classes((IsAuthenticated,))
def StudentAPI(request, *args, **kwargs):
    if request.method == 'GET':
        c = Student.objects.all()
        if kwargs:
            c = Student.objects.get(**kwargs)
            ser = Student_Serializer(c)
        else:
            ser = Student_Serializer(c, many=True)
        return Response(ser.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        serializer = Student_Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        c = Student.objects.get(**kwargs)
        serializer = Student_Serializer(c, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes((BasicAuthentication,))
@permission_classes((IsAuthenticated,))
def MockTestAPI(request, *args, **kwargs):
    header = {'Authorization': 'Token 38258cfd1d0dbf26bc15d78a173600d87c19bfb3'}
    if request.method == 'GET':
        c = MockTest1.objects.all()
        if kwargs:
            c = MockTest1.objects.get(**kwargs)
            ser = Mocktest_Serializer(c)
        else:
            ser = Mocktest_Serializer(c, many=True)
        return Response(ser.data, status=status.HTTP_200_OK,headers=header)
    elif request.method == 'POST':
        serializer = Mocktest_Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'PUT':
        c = MockTest1.objects.get(**kwargs)
        serializer = Mocktest_Serializer(c, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

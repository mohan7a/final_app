from onlineapp.models import *
from rest_framework import serializers


class College_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = College
        fields = ('name', 'location', 'acronym')


class Student_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Student
        fields = ('name', 'college_id', 'email', 'db_folder')


class Mocktest_Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MockTest1
        fields = ('student_id', 'problem1', 'problem2', 'problem3','problem4','total')
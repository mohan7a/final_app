from django.views import View
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.forms import ModelForm
from onlineapp.models import College
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


# Create your views here.
class college_list(View):

    @method_decorator(login_required(login_url='/login_user/'))
    def get(self,request,*args,**kwargs):
        if kwargs:
            colleges_temp = get_object_or_404(College,**kwargs)
            students = list(colleges_temp.student_set.order_by('-mocktest1__total').values('id','name','email','mocktest1__total'))
            return render(
                request,
                template_name='College_Details.html',
                context={
                    'college': colleges_temp,
                    'students': students,
                    'title':'Students from {}|Mentor App'.format(colleges_temp.name),
                    'college_id': colleges_temp.id
                }
            )
        else:
            colleges_temp= College.objects.all()
            temp='Colleges_template.html'
            temp_dict=dict()
            temp_dict['jails']=colleges_temp
            return render(request,context=temp_dict,template_name=temp)


class AddCollegeForm(ModelForm):
    class Meta:
        model=College
        fields=['name','location','acronym','contact']


@login_required(login_url='/login_user/')
def addf(request,*args,**kwargs):
    form=AddCollegeForm(request.POST or None)
    title='Add College'
    submit='Add College'
    if kwargs:
        title = 'Confirm Edit'
        submit = 'Confirm Edit'
        coll=College.objects.get(**kwargs)
        form = AddCollegeForm(request.POST or None,instance=coll)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/colleges/')
    return render(request, 'Add_College.html', {'form': form,'action':submit,'title':title})


@login_required(login_url='/login_user/')
def delf(request,*args,**kwargs):
    coll = College.objects.get(**kwargs)
    form = AddCollegeForm(request.POST or None, instance=coll)
    submit = 'Delete College'
    title = 'Delete College'
    if form.is_valid():
        coll.delete()
        return HttpResponseRedirect('/colleges/')
    return render(request, 'Add_College.html', {'form': form,'action':submit,'title':title})


def test_view(request):
    return HttpResponse("Hello World")
